package com.mariana.datasekolah.network;

import com.mariana.datasekolah.delete.ResponseDelete;
import com.mariana.datasekolah.model.add.ResponseAdd;
import com.mariana.datasekolah.model.login.ResponseLogin;
import com.mariana.datasekolah.model.readSiswa.ResponseReadSiswa;
import com.mariana.datasekolah.model.register.ResponseRegister;
import com.mariana.datasekolah.update.ResponseUpdate;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface ApiInterface
{
    //To Do Login
    @FormUrlEncoded
    @POST("login_user.php")
    Call<ResponseLogin> responseLogin(@Field("vsusername")String vsusername,
                                      @Field("vspassword")String vspassword);

    //To Do Register
    @FormUrlEncoded
    @POST("register_user.php")
    Call<ResponseRegister> responseRegister(@Field("nama_user")String nama_user,
                                            @Field("vsusername")String vsusername,
                                            @Field("vspassword")String vspassword);

    @GET("read_siswa.php")
    Call<ArrayList<ResponseReadSiswa>>actionReadSiswa();


    @PUT("update_siswa.php")
    Call<ResponseUpdate> actionUpdate (@Query("id_siswa") String id_siswa,
                                       @Query("nama_siswa") String nama_siswa,
                                       @Query("kelas") String kelas);


    @DELETE("delete_siswa.php")
    Call<ResponseDelete> actionDelete (@Query("id_siswa") String id);

    @FormUrlEncoded
    @POST("create_siswa.php")
    Call<ResponseAdd> actionAdd (@Field("nama_siswa") String nama_siswa,
                                 @Field("kelas") String kelas);




}

