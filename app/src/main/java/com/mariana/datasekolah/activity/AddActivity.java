package com.mariana.datasekolah.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.mariana.datasekolah.R;
import com.mariana.datasekolah.model.add.ResponseAdd;
import com.mariana.datasekolah.network.ApiClient;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddActivity extends AppCompatActivity {

    @BindView(R.id.editNamaSiswa)
    EditText editNamaSiswa;
    @BindView(R.id.editKelasSiswa)
    EditText editKelasSiswa;
    @BindView(R.id.btnADD)
    Button btnADD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btnADD)
    public void onViewClicked() {
        String nama = editNamaSiswa.getText().toString();
        String kelas = editKelasSiswa.getText().toString();
        if (TextUtils.isEmpty(nama)){
        editNamaSiswa.setError("Jangan Kosong");
    }else if (TextUtils.isEmpty(kelas)){
        editKelasSiswa.setError("Jangan Kosong");
    }else {
            actionAdd(nama, kelas);
        }
}

    private void actionAdd(String nama, String kelas) {
        ApiClient.service.actionAdd(nama, kelas).enqueue(new Callback<ResponseAdd>() {
            @Override
            public void onResponse(Call<ResponseAdd> call, Response<ResponseAdd> response) {
                if (response.isSuccessful()){
                    String messege = response.body().getMessage();
                    String status = response.body().getStatus();
                    if (status.equalsIgnoreCase("1")){
                        Toast.makeText(AddActivity.this, messege, Toast.LENGTH_SHORT);
                        startActivity(new Intent(AddActivity.this,MainActivity.class));
                        finish();
                    }else if (status.equalsIgnoreCase("0")) {
                        Toast.makeText(AddActivity.this, messege, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseAdd> call, Throwable t) {
                Toast.makeText(AddActivity.this, "Gagal OnFaliler", Toast.LENGTH_SHORT).show();

            }
        });
        
    }


}