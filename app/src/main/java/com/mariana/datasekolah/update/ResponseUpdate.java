package com.mariana.datasekolah.update;

import com.google.gson.annotations.SerializedName;


public class ResponseUpdate{

	@SerializedName("nama_siswa")
	private Object namaSiswa;

	@SerializedName("kelas")
	private Object kelas;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	public void setNamaSiswa(Object namaSiswa){
		this.namaSiswa = namaSiswa;
	}

	public Object getNamaSiswa(){
		return namaSiswa;
	}

	public void setKelas(Object kelas){
		this.kelas = kelas;
	}

	public Object getKelas(){
		return kelas;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ResponseUpdate{" + 
			"nama_siswa = '" + namaSiswa + '\'' + 
			",kelas = '" + kelas + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}